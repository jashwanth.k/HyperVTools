write-verbose  "Scan for available updates"
$ci = New-CimInstance -Namespace root/Microsoft/Windows/WindowsUpdate -ClassName MSFT_WUOperationsSession
$result = $ci | Invoke-CimMethod -MethodName ScanForUpdates -Arguments @{SearchCriteria="IsInstalled=0";OnlineScan=$true}
$result.Updates

write-verbose "Install all available updates"
$result = $ci | Invoke-CimMethod -MethodName ApplyApplicableUpdates
#Restart-Computer; exit

write-verbose "Get a list of installed updates"
$result = $ci | Invoke-CimMethod -MethodName ScanForUpdates -Arguments @{SearchCriteria="IsInstalled=1";OnlineScan=$true} 
$result.Updates