#requires -RunAsAdministrator
[cmdletbinding()]
Param()

Get-PSDrive -psprovider FileSystem | ForEach-Object {
    optimize-volume -driveletter  $_.Name -verbose -Defrag
}