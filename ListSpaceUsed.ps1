Get-PSDrive -psprovider FileSystem | ` 
select-object `
    Name, `
    @{Name="Used(Gb)";Expression={ [convert]::ToInt32( $_.Used / 1GB ) } }, `
    @{Name="Free(Gb)";Expression={ [convert]::ToInt32( $_.Free / 1GB )  } }, `
    @{Name="PercentUsed";Expression={ [convert]::ToInt32( 100 * $_.Used / ($_.Used + $_.Free) )}}
