#requires -RunAsAdministrator

[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True,Position=1)]
    [string]$SRV1
)

# Variables
$SRAM = 2GB				                # RAM assigned to Server Operating System
$SRV1VHD = 256GB				                # Size of Hard-Drive for Server Operating System
$VMLOC = "D:\Hyper-V\"			        # Location of the VM and VHDX files
$NetworkSwitch1 = "Hyper-V Virtual Switch"	# Name of the Network Switch

# Create VM Folder and Network Switch
mkdir $VMLOC -ErrorAction SilentlyContinue
$TestSwitch = Get-VMSwitch -Name $NetworkSwitch1 

# Create Virtual Machines
New-VM -Name $SRV1 `
    -Path $VMLOC `
    -MemoryStartupBytes $SRAM `
    -NewVHDPath $VMLOC\$SRV1\$SRV1.vhdx `
    -NewVHDSizeBytes $SRV1VHD `
    -SwitchName $NetworkSwitch1 `
    -Generation 2 `
    -BootDevice NetworkAdapter
