#requires -RunAsAdministrator
Import-Module Hyper-V

$ethernet = Get-NetAdapter -Name ethernet

New-VMSwitch -Name "Hyper-V Virtual Switch" -NetAdapterName $ethernet.Name -AllowManagementOS $true -Notes 'Parent OS, VMs, LAN'
