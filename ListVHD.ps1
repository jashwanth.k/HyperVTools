
Get-PSDrive -psprovider FileSystem | ForEach-Object {

    Get-ChildItem -recurse -path $_.Root -filter *.vhdx `
        | Select-Object `
            fullname, `
            @{Name="Size(Gb)";Expression={ [convert]::ToInt32( $_.Length / 1GB ) } }
   
}