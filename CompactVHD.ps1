#requires -RunAsAdministrator

get-vm | ForEach-Object {

    $vm = $_
    $started = $vm.State -eq 'Running'

    if($started) {
        stop-vm $_ -force
    }

    get-vmharddiskdrive -vm $_ | Where-Object {$_.Path -ne $null} | ForEach-Object {

        $orignalSize = (get-item $_.Path).Length

        Mount-VHD -Path $_.Path -ReadOnly
        Optimize-VHD -Path $_.Path -Mode Full
        Dismount-VHD -Path $_.Path

        $newSize = (get-item $_.Path).Length

        $savingPct = [math]::Round(100 * ($orignalSize - $newSize) / $orignalSize,2)

        $obj = New-Object PSObject
        $obj | Add-Member VMName $vm.Name
        $obj | Add-Member Started $started
        $obj | Add-Member Path $_.Path
        $obj | Add-Member "Original(GB)" ([convert]::ToInt32($orignalSize / 1GB))
        $obj | Add-Member "New(GB)" ([convert]::ToInt32($newSize / 1GB))
        $obj | Add-Member "Saving(GB)" ([convert]::ToInt32(($orignalSize - $newSize) / 1GB))
        $obj | Add-Member SavingPct $savingPct
        $obj 
    }

    if($started) {
        start-vm $_
    }

}