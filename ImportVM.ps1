#requires -RunAsAdministrator
Get-ChildItem . -Recurse -Filter *.xml | ForEach-Object { import-vm $_.FullName -Register }
Get-ChildItem . -Recurse -Filter *.vmcx | ForEach-Object { import-vm $_.FullName -Register }